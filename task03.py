class Temperature:
    def __init__(self, celsius=0):
        """Ініціалізує об'єкт в градусах Цельсія"""
        self.__temperatureCelsius = celsius

    def set_celsius(self, value):
        """Встановлює значення в градусах Цельсія"""
        self.__temperatureCelsius = value

    def set_fahrenheit(self, value):
        """Встановлює значення в градусах Фаренгейта"""
        self.__temperatureCelsius = (value - 32) * 5 / 9

    @property
    def celsius(self):
        """Повертає значення об'єкта в градусах Цельсія"""
        return self.__temperatureCelsius

    @property
    def fahrenheit(self):
        """Повертає значення об'єкта в градусах Фаренгейта"""
        return (self.__temperatureCelsius * 9 / 5) + 32


if __name__ == "__main__":
    temp = Temperature(32)

    print("C:", temp.celsius)
    print("F:", temp.fahrenheit)
    print()

    temp.set_fahrenheit(450)

    print("C:", temp.celsius)
    print("F:", temp.fahrenheit)
