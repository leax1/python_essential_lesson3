class English:
    """Англійська мова"""

    def greeting(self):
        return "Hello friend!"


class Spanish:
    """Іспанська мова"""

    def greeting(self):
        return "Hola amigo!"


def hello_friend(name: str, obj):
    """Функція вітання"""
    print(f"{name}!", obj.greeting())


if __name__ == "__main__":
    english = English()
    spanish = Spanish()

    hello_friend("Alex", english)
    hello_friend("John", spanish)
