class Base:

    @classmethod
    def method(cls):
        print(f"Hello from {cls.__name__}")


class Child(Base):
    pass


if __name__ == "__main__":
    Base.method()
    Child.method()
