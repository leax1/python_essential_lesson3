class Car:
    """Клас Автомобіль"""

    def __init__(self, manufacturer, model, year, color, tank_capacity):
        self.__manufacturer = manufacturer
        self.__model = model
        self.__year = year
        self.__color = color
        self.__tank_capacity = tank_capacity

    def get_manufacturer(self):
        """Повертає назву виробника авто"""
        return self.__manufacturer

    def get_model(self):
        """Повертає модель авто"""
        return self.__model

    def get_year(self):
        """Повертає рік виробництва авто"""
        return self.__year

    def get_color(self):
        """Повертає колір авто"""
        return self.__color

    def set_color(self, color):
        """Змінює колір авто"""
        self.__color = color

    def get_tank_capacity(self):
        """Повертає об'єм паливного баку"""
        return self.__tank_capacity


if __name__ == "__main__":
    car = Car("BMW", "X5", "2023", "Black", 200)

    print(car.get_manufacturer())
    print(car.get_model())
    print(car.get_year())
    print(car.get_color())
    print(car.get_tank_capacity())
